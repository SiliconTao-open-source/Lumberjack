#!/bin/bash

# This script is an addon to Lumberjack.
# If the system does not have Lumberjack installed it will use git to download and install it.

[ $(id -u) -ne 0 ] && {
	echo "Please run as root." 
	echo "sudo $0"
	exit $LINENO
} >&2

DISTRO=debian
[ -f /etc/alpine-release ] && DISTRO=alpine
LOG_FILE=messages

[ "$DISTRO" == "redhad" ] && {
	LOG_FILE="syslog"
} 

function Main {
	InstallLumberjack
	MakeFilter
	InstallHandler
	StartService
	Sample
	cd
}

function Sample {
	[ -f /etc/dhcp/dhcpd.conf ] && {
		echo "# DYNAMIC NAME 1c:98:ec:95:9d:80 HP-Aruba" >> /etc/dhcp/dhcpd.conf
	}
}

function InstallGit {
	which git 2>/dev/null | grep -q git || {
		case "$DISTRO" in
			"debian") 
				apt-get -y install git;;
			"alpine")
				apk add git;;
		esac
	}
}

function InstallLumberjack {
	[ -x /usr/local/bin/Lumberjack.pl ] || {
		InstallGit
		cd /dev/shm/
		git clone https://gitlab.com/SiliconTao-open-source/Lumberjack.git
		cd Lumberjack 
		install -v -m744 -oroot -groot {,/}etc/init.d/Lumberjack
		install -v -m744 -oroot -groot {.,/usr/local/bin}/Lumberjack.pl
		cd eventHandlers
	}
}

function MakeFilter {
	mkdir -pv /etc/Lumberjack.d/
	CONF=/etc/Lumberjack.d/$LOG_FILE
	[ -f $CONF ] || {
		echo "LOG_FILE=/var/log/$LOG_FILE" > $CONF
	}
	grep DynamicHostnameRegistration.sh $CONF || {
		echo "LUMBERJACK FILTER m/DHCPREQUEST/ LUMBERJACK ACTION /usr/local/bin/DynamicHostnameRegistration.sh %LINE" >> $CONF
	}
}

function InstallHandler {
	install -v -m744 -oroot -groot {./,/usr/local/bin/}DynamicHostnameRegistration.sh
}

function StartService {
	case "$DISTRO" in
		"debian") 
			service enable Lumberjack;
			service stop Lumberjack;
			service start Lumberjack;;
		"alpine")
			rc-update add Lumberjack;
			rc-service Lumberjack stop;
			rc-service Lumberjack start;;
	esac
}

Main

