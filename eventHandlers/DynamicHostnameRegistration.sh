#!/bin/bash

# This scripe will set a hostname in named for preconfigured MAC addresses.
# This will allow the system to assign names at DHCP request time while allowing the IP to be assigned from the pool.

# Lumberjack will pass a log line like so
# Oct 25 14:24:09 ns1 daemon.info dhcpd[446]: DHCPREQUEST for 172.16.1.108 from 1c:98:ec:95:9d:80 via eth0
# Oct 25 13:11:25 ns1 daemon.info dhcpd[446]: DHCPREQUEST for 172.16.0.108 (172.16.0.100) from e8:e8:b7:66:66:b2 (Galaxy-S10) via eth0
# It will match lines that have a MAC address in the /etc/dhcp/dhcpd.conf file.
# MAC addresses that are found as comment lines will have have their named registration changed to the name in the config file.
# Format of the renaming lines starting with the comment
# DYNAMIC NAME <mac address> <name to assign>

INPUT="$1"
# INPUT="Oct 25 14:24:09 ns1 daemon.info dhcpd[446]: DHCPREQUEST for 172.16.1.108 from 1c:98:ec:95:9d:80 via eth0"
echo "INPUT = '$INPUT'"
IP_MAC=( $(echo "$INPUT" | sed -e 's/.*DHCPREQUEST for //; s/ from /\n/' | awk '{print $1}') )

#echo "IP = ${IP_MAC[0]}"
#echo "MAC = ${IP_MAC[1]}"

NEW_NAME=$(grep ^"# DYNAMIC NAME ${IP_MAC[1]}" /etc/dhcp/dhcpd.conf | awk '{print $NF}' | grep -i "[a-z]") && {
	DOMAIN=$(grep ^zone /etc/dhcp/dhcpd.conf|awk '{print $2}' | grep -i "^[a-z]") 
	/usr/local/bin/AddNsRecord.sh ${NEW_NAME}.${DOMAIN} ${IP_MAC[0]}
}


 

