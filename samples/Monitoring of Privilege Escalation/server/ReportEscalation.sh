#!/bin/bash

GOOD=0
WARNING=1
CRITICAL=2
ALERT_STATUS=$CRITICAL

ADMIN_USER=$(echo $@ | sed -e 's/^.*status=mesg/status=mesg/'|awk '{print $7}')
EVENT_TIME=$(echo $@ | sed -e 's/^.*status=mesg/status=mesg/'|awk '{print $2,$3,$4}')
CMD="[$(date +%s)] PROCESS_SERVICE_CHECK_RESULT;REPLACE_HOSTNAME_OF_SERVER;Privilege_Escalation;$ALERT_STATUS;$ADMIN_USER $EVENT_TIME"

echo $CMD | logger -T Privilege_Escalation
echo $CMD >> /var/spool/icinga/cmd/icinga.cmd
