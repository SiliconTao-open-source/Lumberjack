#!/bin/bash

LOG=/dev/shm/$(basename $0).log
date >> $LOG
echo $0 >> $LOG
while [ $# -gt 0 ]; do
	NEXT_ARG=$1
	shift
	echo "NEXT_ARG=$NEXT_ARG"
done >> $LOG
echo ----------------- >> $LOG
echo >> $LOG

sleep 3