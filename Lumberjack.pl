#!/usr/bin/perl

# The lumberjack cuts up logs and sends them off for processing.
#
# This script activly monitors a log file and triggers any number of custom events when it finds a matching line.
#
# This script accepts one argument, the config file to read from.
# In the config file there are two types of lines, one LOG_FILE line and one or more filter lines.
# The LOG_FILE setting tells lumberjack what log file to read from
# The filter lines are in the format of 
# LUMBERJACK FILTER filter LUMBERJACK ACTION ActionScript [ARG1] [ARG2] .. [ARGn]
# Where filter is a Perl formated regular expression.
# Where ActionScript is any program or script in the system
# Arguments are given to the ActionScript
# The special argument of %LINE will pass the entire line from the log file that the filter matched as an argument to the ActionScript
#
# The config file can be changed in real time, Lumberjack will detect the change and re-read all filters.
# 
# Please see the samples directory for examples of ActionScript
# Please see the etc/Lumberjack.d/ directory for examples of config files.

use Fcntl qw(SEEK_SET SEEK_CUR SEEK_END);
#use File::stat;

# This is to prevent fork from creating zombie processes
$SIG{CHLD} = 'IGNORE';
my $Done = 0;
my $LogFile = "";
my $ConfFile = shift @ARGV;
print "The conf file $ConfFile\n";
my @FilterActions;

sub LoadFilters {
	open CF, "< $ConfFile";
	undef @FilterActions;
	while (<CF>) {
		if (/^LUMBERJACK /) {
			my $ReadLine = $_;
			chomp $ReadLine;
			my $NewFilter;
			my @NewAction;
			if ( $ReadLine =~ m/LUMBERJACK FILTER (.*?) LUMBERJACK ACTION/ ) {
				$NewFilter = $1;
				my $Pos = index($ReadLine, "LUMBERJACK ACTION");
				#print "substr $Pos ----- ".substr ( $ReadLine, ((length($ReadLine) - $Pos - 18 ) * -1))."\n\n";								
				@NewAction = split / /, substr $ReadLine, ((length($ReadLine) - $Pos - 18 ) * -1);
				chomp $NewFilter;
				print "Loading filter '$NewFilter'\n";
				push @FilterActions, [ $NewFilter, @NewAction ];
			}
		}
		if(/^LOG_FILE=/) {
			$LogFile = substr $_, -(length($_) - 9) ;
			chomp $LogFile;
		}
	}
	# print "close conf\n";
	close CF;
}

sub Main {
	# print "Read conf file $ConfFile\n";
	LoadFilters;
	
	print "LOG_FILE='$LogFile'\n";
	
	my $ConfAge = (stat($ConfFile))[9];
	my $LogFileSize = 0;

	open FILEHANDLE, "<", $LogFile;
	seek FILEHANDLE, 0, SEEK_END;

	while ( $Done == 0 ) {
		while(<FILEHANDLE>) {
			my $Line = $_;
			chomp $Line;
			#print "Line = '$Line'\n";
			for my $i (0 .. $#FilterActions) {
				#print "eval $i ".$FilterActions[$i][0]." then exec ".$FilterActions[$i][1]."\n";
				my $Filter = $FilterActions[$i][0];
				my $Command = sprintf "\$Line =~ $Filter";
				if ( eval "$Command" ) {
					# print "Found match go run $FilterActions[$i][1] arg count is $#{$FilterActions[$i]}\n";
					my @CmdStack;
					foreach my $iCmd ( 1 .. $#{$FilterActions[$i]} ) {
						my $CmdArg = ${$FilterActions[$i]}[$iCmd];
						if ($CmdArg eq "%LINE" ) {
							# print "replace line with the data in the line\n";
							push @CmdStack, $Line; 
						} else {
							# print "CmdArg = '".${$FilterActions[$i]}[$iCmd]."'\n\n";
							push @CmdStack, ${$FilterActions[$i]}[$iCmd];
						}
					}
					unless ( fork () ) {
						system ( @CmdStack ); 
						exit 0;
					}
					#print "CmdArg = '".${$FilterActions[$i]}[$iCmd]."'\n\n";
				} 
			}
		}
		sleep 1;
		my $NewAge = (stat($ConfFile))[9];
		if ($NewAge > $ConfAge ) {
			$ConfAge = $NewAge;
			LoadFilters;
		}
		
		# Logrotate
		my $NewFileSize = (stat($LogFile))[7];
		if ( $NewFileSize < $LogFileSize ) {
			print "file truncated, reset position\n";
			close FILEHANDLE;
			open FILEHANDLE, "<", $LogFile;
			seek FILEHANDLE, 0, SEEK_END;
		}
		$LogFileSize = $NewFileSize;

		# Look for request to end
	}

	close FILEHANDLE;
}

Main;
